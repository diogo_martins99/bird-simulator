﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Game Data", menuName = "Scriptable Objects/Game Data")]
public class GameData : ScriptableObject
{
    [SerializeField] private float spawnerDistance;
    [SerializeField] private float obstacleInterval;

    public float SpawnerDistance => spawnerDistance;
    public float ObstacleInterval => obstacleInterval;
}
