﻿using UnityEngine;
using Cinemachine;

public class Breakable : MonoBehaviour
{
    [Header("Collision Effect")]
    [SerializeField] protected float minExplosionForce = 300;
    [SerializeField] protected float maxExplosionForce = 500;
    [SerializeField] protected float extraForceMultiplier = 2;
    [SerializeField] protected float explosionRadius = 10;
    [SerializeField] protected GameObject brokenPrefab;
    [SerializeField] protected bool useAnimationInstead;

    [Header("References")]
    [SerializeField] protected CinemachineImpulseSource impulseSource;

    [Header("Other")]
    [SerializeField] protected float destroyDelay = 5;
    protected void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {

            PlayerController player = other.gameObject.GetComponentInParent<PlayerController>();
            player.Data.OnBreakableCollision.Invoke();

            Break(other.transform.position, player.Data.IsInvincible);
        }
    }

    protected virtual void Break(Vector3? contactPoint = null, bool applyExtraForce = false)
    {

        if (impulseSource != null)
        {
            float impulsePower = applyExtraForce ? 1 : 0.2f;
            impulseSource.m_ImpulseDefinition.m_AmplitudeGain = impulsePower;
            //impulseSource.m_ImpulseDefinition.m_FrequencyGain = impulsePower;
            impulseSource.GenerateImpulse();
        }

        if (brokenPrefab != null && !useAnimationInstead)
        {
            GameObject brokenObj = InstantiatePrefab();
            Vector3 explosionPosition = contactPoint != null ? (Vector3)contactPoint : transform.position;

            float forceMultiplier = applyExtraForce ? extraForceMultiplier : 1;

            foreach (Rigidbody rb in brokenObj.GetComponentsInChildren<Rigidbody>())
            {
                rb.AddExplosionForce(Random.Range(minExplosionForce, maxExplosionForce) * forceMultiplier,
                                     explosionPosition,
                                     explosionRadius);

                rb.transform.parent = null;

                Destroy(rb.gameObject, destroyDelay);
            }
            gameObject.SetActive(false);
            Destroy(gameObject, destroyDelay);
        }
        else if (useAnimationInstead)
        {
            Animator anim = GetComponent<Animator>();

            if (anim != null) anim.SetTrigger("Break");
        }
    }

    protected virtual GameObject InstantiatePrefab()
    {
        GameObject go = Instantiate(brokenPrefab, transform.position, transform.rotation);
        return go;
    }
}
