﻿using UnityEngine;

public class Obstacle : Breakable
{
    [Header("Obstacle Size Variation")]
    [SerializeField] private float sizeIncreaseChance = 20f;
    [SerializeField] private int[] sizeMultipliers;

    private float sizeMultiplier;
    protected void Start()
    {

        if (Random.Range(0f, 100f) < sizeIncreaseChance)
        {
            sizeMultiplier = sizeMultipliers[Random.Range(0, sizeMultipliers.Length)];
            transform.localScale = new Vector3(transform.localScale.x,
                                               transform.localScale.y * sizeMultiplier,
                                               transform.localScale.z);

        }
    }
    protected override GameObject InstantiatePrefab()
    {
        GameObject go = Instantiate(brokenPrefab, transform.position, transform.rotation);

        go.transform.localScale = new Vector3(go.transform.localScale.x,
                                              transform.localScale.y,
                                              go.transform.localScale.z);


        return go;
    }

}
