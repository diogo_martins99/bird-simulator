﻿using UnityEngine;

public class ObstacleSet : MonoBehaviour
{
    [SerializeField] private Transform childObjectsTransform;
    [SerializeField] private float minOffset = 15, maxOffset = 45;
    void Start()
    {
        childObjectsTransform.localPosition = new Vector3(childObjectsTransform.localPosition.x,
                                                       Random.Range(minOffset, maxOffset),
                                                       childObjectsTransform.localPosition.z);
    }
}
