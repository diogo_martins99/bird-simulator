﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickable : MonoBehaviour
{

    protected Animator anim;
    protected void Awake()
    {
        anim = GetComponent<Animator>();
    }
    protected void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            GetPicked();

            PlayerController player = other.gameObject.GetComponentInParent<PlayerController>();
            
            if (player != null)
            {
                player.Data.OnPickableCollision.Invoke();
                RewardPlayer(player);
            }
        }
    }

    protected virtual void GetPicked()
    {
        if (anim != null) anim.SetTrigger("Picked");
    }

    protected virtual void RewardPlayer(PlayerController player)
    {

    }
}
