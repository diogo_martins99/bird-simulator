﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Pickable
{
    [SerializeField] private ParticleSystem[] coinVFX;

    protected override void GetPicked()
    {
        base.GetPicked();
        foreach (ParticleSystem vfx in coinVFX) vfx.Play();
    }

    protected override void RewardPlayer(PlayerController player)
    {
        base.RewardPlayer(player);
        player.GetCoin();
    }
}
