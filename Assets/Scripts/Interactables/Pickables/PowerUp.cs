﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : Pickable
{
    [SerializeField] private float invencibilityTime = 10;
    [SerializeField] private ParticleSystem explosionVFX;

    protected override void GetPicked()
    {
        base.GetPicked();

        if (explosionVFX != null) explosionVFX.Play();
    }
    protected override void RewardPlayer(PlayerController player)
    {
        base.RewardPlayer(player);

        player.BecomeInvencible(invencibilityTime);

    }
}
