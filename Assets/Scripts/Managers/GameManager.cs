﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private bool startInput;
    private void Start()
    {
        Managers.UIManager.ShowStartMessage(true);
        Time.timeScale = 0;
    }

    private void Update()
    {
        // Check for input before starting the prototype
        if (!startInput && Input.GetKeyDown(KeyCode.Space))
        {
            Time.timeScale = 1;
            Managers.UIManager.ShowStartMessage(false);
        }

        if (Input.GetKeyDown(KeyCode.Escape)) GoToMenu();
    }

    public void Restart() => SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    private void GoToMenu() => SceneManager.LoadScene(0);
}
