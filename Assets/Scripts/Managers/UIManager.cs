﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    [Header("Game State")]
    [SerializeField] private GameObject startMessagePanel;
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private TMP_Text statsText;
    [Header("Player")]
    [SerializeField] private Image invencibilityTime;
    [Header("Debug")]
    [SerializeField] private TMP_Text physicsDebugText;
    [SerializeField] private Slider inputMultiplierSlider;
    [SerializeField] private TMP_Text sliderValueText;
    [SerializeField] private RectTransform cleanInputTime;
    [SerializeField] private RectTransform multipliedInputTime;

    private void Start()
    {
        UpdateSliderValue();
    }

    public void ShowStartMessage(bool show) => startMessagePanel.SetActive(show);
    public void ShowGameOverPanel(bool show) => gameOverPanel.SetActive(show);

    public void UpdateSliderValue()
    {
        sliderValueText.text = inputMultiplierSlider.value.ToString();
    }

    public void SetInputTimeImagesSize(float inputTime, float multipliedTime)
    {
        cleanInputTime.sizeDelta = new Vector2(inputTime * 100, cleanInputTime.sizeDelta.y);
        multipliedInputTime.sizeDelta = new Vector2(multipliedTime * 100, cleanInputTime.sizeDelta.y);
    }

    public void ShowInvencibilityBar(bool show) => invencibilityTime.transform.parent.gameObject.SetActive(show);

    public void UpdateInvencibilityBar(float currentTime, float maxTime)
    {
        invencibilityTime.fillAmount = currentTime / maxTime;
    }


    public void SetStatsText(float distance, float coins, float wallsBroken)
    {
        statsText.text = "Distance: " + Mathf.Round(distance) +
                                "\n" +
                                "Coins Picked: " + coins +
                                "\n" +
                                "Walls Broken: " + wallsBroken;
    }
    public void SetPhysicsDebugText(float speed, float acceleration, float drag, float rotation)
    {
        physicsDebugText.text = "Speed: " + Mathf.Round(speed) +
                                "\n" +
                                "Acceleration: " + Mathf.Round(acceleration) +
                                "\n" +
                                "Drag: " + Mathf.Round(drag) +
                                "\n" +
                                "Rotation: " + Mathf.Round(rotation);
    }
}
