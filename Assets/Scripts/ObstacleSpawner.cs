﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{

    [SerializeField] private GameData gameData;
    [SerializeField] private PlayerData playerData;
    [SerializeField] private GameObject[] obstacleSets;
    [SerializeField] private Transform obstaclesParent;
    [SerializeField] private float destroyDelay = 20;

    private int spawnFlag = 1;
    // Start is called before the first frame update
    void Start()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, gameData.SpawnerDistance);
        StartCoroutine(SpawnObstacles());
    }


    private IEnumerator SpawnObstacles()
    {
        while (true)
        {
            if (playerData.CurrentDistance >= gameData.ObstacleInterval * spawnFlag)
            {
                SpawnRandomObstacle();
                spawnFlag++;
            }
            yield return null;
        }
    }

    private void SpawnRandomObstacle()
    {
        if (obstacleSets == null || obstacleSets.Length == 0) return;

        GameObject go = Instantiate(obstacleSets[Random.Range(0, obstacleSets.Length)], transform.position, transform.rotation, obstaclesParent);

        Destroy(go, destroyDelay);

        //if (spawnFlag % 6 == 0)
        //Instantiate(floor, transform.position, transform.rotation, obstaclesParent);
    }
}
