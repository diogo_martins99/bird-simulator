﻿using UnityEngine;
using Cinemachine;

public class PlayerCameraController : MonoBehaviour
{
    [SerializeField] private PlayerData data;
    [SerializeField] private CinemachineVirtualCamera cam;

    private CinemachineComposer composer;
    private float initCameraOffset;
    private float initFOV;

    private void Start()
    {
        composer = cam.GetCinemachineComponent<CinemachineComposer>();
        initCameraOffset = composer.m_TrackedObjectOffset.x;
        initFOV = cam.m_Lens.FieldOfView;
    }

    private void Update()
    {
        UpdateFOV();
        UpdateCameraLookOffset();
    }
    private void UpdateFOV()
    {
        cam.m_Lens.FieldOfView = Utility.Lerp(data.CurrentRotation, data.MinXRotation, data.MaxXRotation, initFOV - 5, initFOV + 5);
    }

    private void UpdateCameraLookOffset()
    {
        composer.m_TrackedObjectOffset.x = Utility.Lerp(data.CurrentRotation, data.MinXRotation, data.MaxXRotation, initCameraOffset, initCameraOffset + 5);
    }
}
