﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionController : MonoBehaviour
{
    [SerializeField] private PlayerController playerController;

    private void Awake()
    {
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Bounds"))
        {
            playerController.Data.OnBoundsCollision.Invoke();
        }
    }
}
