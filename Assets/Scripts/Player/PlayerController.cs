﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private PlayerData data;
    [SerializeField] private PlayerSaveData saveData;
    [SerializeField] private PlayerPhysicsController physicsController;
    [SerializeField] private Animator birdAnim;
    private float currentHp;
    private bool isDead;
    private int currentCoins = 0;
    private int currentWallsBroken = 0;
    private float currentInvincibleTime = 0;
    private Coroutine invincibleCoroutine;
    private Material initMaterial;
    public PlayerData Data => data;

    private void Awake()
    {
        data.ResetStats();
    }
    private void Start()
    {
        currentHp = data.Hp;
    }

    private void OnEnable()
    {
        data.OnBreakableCollision += HitObstacle;
        data.OnObstacleCollision += HitObstacle;
        data.OnPickableCollision += Celebrate;
        data.OnBoundsCollision += Die;

        data.OnInputValueChange += UpdateWingFlapAnim;
    }
    private void OnDisable()
    {
        data.OnBreakableCollision -= HitObstacle;
        data.OnObstacleCollision -= HitObstacle;
        data.OnPickableCollision -= Celebrate;
        data.OnBoundsCollision -= Die;

        data.OnInputValueChange -= UpdateWingFlapAnim;
    }

    private void HitBreakable()
    {
        Celebrate();
    }
    private void HitObstacle()
    {
        LoseHP();
        currentWallsBroken++;
        physicsController.DecreaseSpeed(data.CollisionSpeedDecrease);
    }
    private void Celebrate()
    {
        birdAnim.SetInteger("CelebrationIndex", UnityEngine.Random.Range(0, 3));
        birdAnim.SetTrigger("Celebration");
    }

    public void BecomeInvencible(float duration)
    {
        currentInvincibleTime = duration;
        if (invincibleCoroutine == null)
            invincibleCoroutine = StartCoroutine(ActivateInvencibility(duration));

    }

    private IEnumerator ActivateInvencibility(float maxTime)
    {
        data.UpdateInvincibilityState(true);
        Managers.UIManager.ShowInvencibilityBar(true);

        while (currentInvincibleTime > 0)
        {
            Managers.UIManager.UpdateInvencibilityBar(currentInvincibleTime, maxTime);
            currentInvincibleTime -= Time.deltaTime;
            yield return null;
        }
        //Celebrate();
        data.UpdateInvincibilityState(false);
        Managers.UIManager.ShowInvencibilityBar(false);
        invincibleCoroutine = null;
    }

    private void LoseHP()
    {
        if (!data.HasHp || data.IsInvincible) return;

        if (currentHp > 1) currentHp--;
        else Die();
    }

    private void Die()
    {
        if (data.IsInvincible || isDead) return;

        isDead = true;

        InstantiateRagdoll();

        StartCoroutine(ShowGameOverPanelAfterDelay(2.5f));

        Save();
    }

    private IEnumerator ShowGameOverPanelAfterDelay(float delay)
    {

        yield return new WaitForSeconds(delay);

        Managers.UIManager.SetStatsText(data.CurrentDistance, currentCoins, currentWallsBroken);
        Managers.UIManager.ShowGameOverPanel(true);
    }

    private void InstantiateRagdoll()
    {
        GameObject ragdoll = Instantiate(data.DeathRagdollPrefab, data.BirdTransform.position, data.BirdTransform.rotation);

        data.BirdTransform.gameObject.SetActive(false);

        //ragdoll.transform.position += Vector3.up;

        foreach (Rigidbody rb in ragdoll.GetComponentsInChildren<Rigidbody>())
        {
            rb.AddExplosionForce(100, ragdoll.transform.position - Vector3.forward * 2, 5);
        }
    }

    private void Save()
    {
        saveData.UpdateCoinsAmount(currentCoins);
    }

    // Add one coin to coin counter
    public void GetCoin() => currentCoins++;

    private void UpdateWingFlapAnim()
    {
        birdAnim.SetBool("FlapWings", data.IsFlappingWings);
    }
}
