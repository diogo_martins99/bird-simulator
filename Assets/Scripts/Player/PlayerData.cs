﻿using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "Bird Data", menuName = "Scriptable Objects/Bird Data")]
public class PlayerData : ScriptableObject
{
    [Header("General")]
    [SerializeField] private bool hasHp = false;
    [SerializeField] private int hp = 5;

    [Header("Input")]
    [SerializeField] private float inputTimeMultiplier = 1;
    [SerializeField] private float minInputTimeMultiplier = 1;
    [SerializeField] private float maxInputTimeMultiplier = 4;
    [SerializeField] private float minSpeedToStopInput = 5f;

    [Header("Physics")]
    [SerializeField] private float minSpeed = 0f;
    [SerializeField] private float maxSpeed = 10f;
    [SerializeField] private float invincibilityMaxSpeed = 90;
    [SerializeField] private float minAcceleration = -20f;
    [SerializeField] private float maxAcceleration = 10f;
    [SerializeField] private float minDrag = 0f;
    [SerializeField] private float maxDrag = 5f;

    [Header("Rotation")]
    [SerializeField] private float minXRotation = -90f;
    [SerializeField] private float maxXRotation = 90f;
    [SerializeField] private float upwardRotationAcceleration = -10f;
    [SerializeField] private float downwardRotationAcceleration = 2f;

    [Header("Collisions")]
    [SerializeField] private float collisionSpeedDecrease = 50;

    [Header("References")]
    [SerializeField] private GameObject deathRagdollPrefab;
    [SerializeField] private Material invincibleMaterial;

    // Public properties with no setters
    public bool HasHp => hasHp;
    public float Hp => hp;
    public float CurrentInputMultiplier => inputTimeMultiplier;
    public float MinInputTimeMultiplier => minInputTimeMultiplier;
    public float MaxInputTimeMultiplier => maxInputTimeMultiplier;
    public float MinSpeedToStopInput => minSpeedToStopInput;
    public float MinSpeed => minSpeed;
    public float InvecibilityMaxSpeed => invincibilityMaxSpeed;
    public float MinAcceleration => minAcceleration;
    public float MaxAcceleration => maxAcceleration;
    public float MinXRotation => minXRotation;
    public float MaxXRotation => maxXRotation;
    public float MinDrag => minDrag;
    public float MaxDrag => maxDrag;
    public float UpwardRotationAcceleration => upwardRotationAcceleration;
    public float DownwardRotationAcceleration => downwardRotationAcceleration;
    public float CollisionSpeedDecrease => collisionSpeedDecrease;
    public GameObject DeathRagdollPrefab => deathRagdollPrefab;
    public Material InvincibleMaterial => invincibleMaterial;

    // Public properties with private setters
    public float MaxSpeed { get; private set; }
    public float CurrentSpeed { get; private set; }
    public float CurrentRotation { get; private set; }
    public float CurrentDistance { get; private set; }
    public bool IsInvincible { get; private set; }
    public bool IsFlappingWings { get; private set; }
    public bool IsAtHighSpeed { get; private set; }
    public Transform BirdTransform { get; set; }
    public UnityAction OnHighSpeedChange, OnInputValueChange, OnInvincibilityValueChange;
    public UnityAction OnBreakableCollision, OnObstacleCollision, OnPickableCollision, OnBoundsCollision;

    public void ResetStats()
    {
        MaxSpeed = maxSpeed;
        IsInvincible = false;
        IsFlappingWings = false;
        IsAtHighSpeed = false;
        CurrentSpeed = 0;
        CurrentRotation = 0;
        CurrentDistance = 0;
    }

    public void UpdateInputData(float inputMultiplier, bool isFlappingWings)
    {
        inputTimeMultiplier = Mathf.Clamp(inputMultiplier, minInputTimeMultiplier, maxInputTimeMultiplier);
        IsFlappingWings = isFlappingWings;

        if (OnInputValueChange != null) OnInputValueChange.Invoke();
    }

    public void UpdatePlayerStats(float speed, float rotation, float travelledDistance)
    {
        CurrentSpeed = speed;
        CurrentRotation = rotation;
        CurrentDistance = travelledDistance;

        // If the player is pointing down and has at least a third of its max speed set IsAtHighSpeed to true
        if (CurrentRotation > 0 && CurrentSpeed > MaxSpeed / 3 && !IsAtHighSpeed)
        {
            IsAtHighSpeed = true;
            if (OnHighSpeedChange != null) OnHighSpeedChange.Invoke();
        }
        else if (IsAtHighSpeed)
        {
            IsAtHighSpeed = false;

            if (OnHighSpeedChange != null) OnHighSpeedChange.Invoke();
        }
    }

    public void UpdateInvincibilityState(bool isInvincible)
    {
        IsInvincible = isInvincible;
        OnInvincibilityValueChange.Invoke();

        MaxSpeed = isInvincible ? invincibilityMaxSpeed : maxSpeed;
    }

}
