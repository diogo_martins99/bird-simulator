﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;

public class PlayerEffectsController : MonoBehaviour
{
    [SerializeField] private PlayerData data;
    [SerializeField] private TrailRenderer[] trails;
    [SerializeField] private GameObject[] highSpeedEffects;
    [SerializeField] private Renderer[] birdRenderers;
    [SerializeField] private TrailRenderer[] superTrails;
    [SerializeField] private ParticleSystem[] superVFX;
    [SerializeField] private ParticleSystem explosionVFX;

    private List<float> superTrailsInitialSize;
    private List<float> superVFXInitialSpeed;
    private List<float> superVFXInitialLifetime;
    private Material initMaterial;


    private void Start()
    {
        initMaterial = birdRenderers[0].material;

        GetSuperVFXInitialSize();
        GetSuperTrailsInitialSize();
    }

    private void OnEnable()
    {
        data.OnHighSpeedChange += OnHighSpeedChange;
        data.OnInvincibilityValueChange += ChangeBirdMat;
        data.OnInvincibilityValueChange += ShowInvincibleEffects;
        data.OnBreakableCollision += PlayExplosionVFX;
    }
    private void OnDisable()
    {
        data.OnHighSpeedChange -= OnHighSpeedChange;
        data.OnInvincibilityValueChange -= ChangeBirdMat;
        data.OnInvincibilityValueChange -= ShowInvincibleEffects;
        data.OnBreakableCollision -= PlayExplosionVFX;
    }

    private void OnHighSpeedChange()
    {
        foreach (GameObject go in highSpeedEffects) go.SetActive(data.IsAtHighSpeed);
    }
    private void Update()
    {
        ModifyTrail();
    }
    private void ModifyTrail()
    {
        foreach (TrailRenderer tr in trails)
        {
            tr.time = (data.CurrentSpeed) * 0.03f;
        }
    }

    private void PlayExplosionVFX()
    {
        if (data.IsInvincible) explosionVFX.Play();
    }
    private void ShowInvincibleEffects()
    {
        ChangeBirdMat();
        ShowInvincibleTrails();
        ShowInvincibleParticles();

        if (data.IsInvincible) StartCoroutine(ModifySuperEffects());
    }

    private void ChangeBirdMat()
    {
        foreach (Renderer rend in birdRenderers)
        {
            rend.material = data.IsInvincible ? data.InvincibleMaterial : initMaterial;
        }
    }

    private void ShowInvincibleTrails()
    {
        int i = 0;
        foreach (TrailRenderer tr in superTrails)
        {
            tr.gameObject.SetActive(data.IsInvincible);
            tr.time = data.IsInvincible ? 0 : superTrailsInitialSize[i];
            i++;
        }
    }
    private void ShowInvincibleParticles()
    {
        foreach (ParticleSystem vfx in superVFX)
        {
            if (data.IsInvincible) vfx.Play();
            else vfx.Stop();
        }
    }

    private void GetSuperTrailsInitialSize()
    {
        superTrailsInitialSize = new List<float>(superTrails.Length);

        foreach (TrailRenderer rend in superTrails)
        {
            superTrailsInitialSize.Add(rend.time);
        }
    }
    private void GetSuperVFXInitialSize()
    {
        superVFXInitialSpeed = new List<float>(superVFX.Length);
        superVFXInitialLifetime = new List<float>(superVFX.Length);


        foreach (ParticleSystem ps in superVFX)
        {
            superVFXInitialSpeed.Add(ps.main.startSpeedMultiplier);
            superVFXInitialLifetime.Add(ps.main.startLifetimeMultiplier);
        }
    }

    private IEnumerator ModifySuperEffects()
    {
        while (data.IsInvincible)
        {
            int i = 0;
            foreach (TrailRenderer tr in superTrails)
            {
                tr.time = superTrailsInitialSize[i] * (data.CurrentSpeed) * 0.02f;
                i++;
            }

            /*i = 0;
            ParticleSystem.MainModule psmain;
            foreach (ParticleSystem ps in superVFX)
            {
                psmain = ps.main;
                psmain.startSpeedMultiplier = superVFXInitialSpeed[i] * (data.CurrentSpeed) * 0.01f;
                psmain.startLifetimeMultiplier = superVFXInitialLifetime[i] * (data.CurrentSpeed) * 0.01f;
                i++;
            }*/

            yield return null;
        }
    }



}
