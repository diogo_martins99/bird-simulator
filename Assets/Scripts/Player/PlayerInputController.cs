﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInputController : MonoBehaviour
{
    [SerializeField] private PlayerData data;
    [SerializeField] private Slider inputMultiplierSlider;
    private float inputMultiplier;
    private float cleanInputTime;
    private bool isFlappingWings = false;
    private bool inputValueChanged = false;
    private bool pressing = false;

    private void Start()
    {
        inputMultiplier = data.CurrentInputMultiplier;
        SetInitialSliderValues();
    }

    private void SetInitialSliderValues()
    {
        inputMultiplierSlider.minValue = data.MinInputTimeMultiplier;
        inputMultiplierSlider.maxValue = data.MaxInputTimeMultiplier;
        inputMultiplierSlider.value = inputMultiplier;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Managers.GameManager.Restart();
        }
        if (Input.GetKey(KeyCode.Space) && !pressing && data.CurrentSpeed >= data.MinSpeedToStopInput / 2)
        {
            pressing = true;
            StopAllCoroutines();
            StartCoroutine(MultiplyInputTime());
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            pressing = false;
            inputValueChanged = true;
        }


        // Only call SetInputData() when isFlappingWings value changes
        if (inputValueChanged)
        {
            // Update input data
            data.UpdateInputData(inputMultiplier, isFlappingWings);
            inputValueChanged = false;
        }
    }

    private IEnumerator MultiplyInputTime()
    {
        cleanInputTime = 0;
        isFlappingWings = true;
        inputValueChanged = true;

        float multipliedTime = 0;

        // Keep counting pressed input time
        while (pressing)
        {
            // Stop input if bird's speed is too low
            if (data.CurrentSpeed <= data.MinSpeedToStopInput)
            {
                pressing = false;
                cleanInputTime = 0;
                multipliedTime = 0;
            }


            Managers.UIManager.SetInputTimeImagesSize(cleanInputTime, multipliedTime);
            cleanInputTime += Time.deltaTime;
            multipliedTime = cleanInputTime * inputMultiplier;
            yield return null;
        }

        // Countdown
        while (multipliedTime > cleanInputTime)
        {

            // Stop input if bird's speed is too low
            if (data.CurrentSpeed <= data.MinSpeedToStopInput)
            {
                cleanInputTime = 0;
                multipliedTime = 0;
            }
            Managers.UIManager.SetInputTimeImagesSize(cleanInputTime, multipliedTime);
            multipliedTime -= Time.deltaTime;
            yield return null;
        }

        cleanInputTime = 0;
        multipliedTime = 0;
        Managers.UIManager.SetInputTimeImagesSize(cleanInputTime, multipliedTime);

        isFlappingWings = false;
        inputValueChanged = true;

    }


    //Called when the the UI inputSlider's value is changed
    public void UpdateInputMultiplier(float newMultiplier)
    {
        inputMultiplier = newMultiplier;

        // Update input data
        data.UpdateInputData(inputMultiplier, isFlappingWings);
    }
}
