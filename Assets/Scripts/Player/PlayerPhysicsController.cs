﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPhysicsController : MonoBehaviour
{
    [SerializeField] private PlayerData data;
    [SerializeField] private Transform birdTransform;
    [SerializeField] private Rigidbody birdRigidbody;
    private float currentHp;
    private float totalSpeed = 0, forwardSpeed, upwardSpeed;
    private float totalAcceleration;
    private float rotationSpeed = 15;
    private float xRotation;
    private float drag;
    private float travelledDistance = 0;

    private void Start()
    {
        totalSpeed = data.MaxSpeed * 0.6f;
        data.BirdTransform = birdTransform;
    }

    private void OnEnable()
    {
        data.OnInvincibilityValueChange += () => totalSpeed = data.InvecibilityMaxSpeed * 0.8f;
    }
    private void OnDisable()
    {
        data.OnInvincibilityValueChange -= () => totalSpeed = data.InvecibilityMaxSpeed * 0.8f;
    }
    private void Update()
    {
        UpdateRotation();
        UpdateAcceleration();
        UpdateSpeed();
        UpdateDrag();

        data.UpdatePlayerStats(totalSpeed, xRotation, travelledDistance);

        Managers.UIManager.SetPhysicsDebugText(totalSpeed, totalAcceleration, birdRigidbody.drag, xRotation);

    }

    //Decrease current speed to a percentage of it
    public void DecreaseSpeed(float percentage = 80)
    {
        if (data.IsInvincible) return;

        totalSpeed = totalSpeed * percentage / 100;
    }

    private void UpdateRotation()
    {
        if (data.IsFlappingWings)
        {
            // xRotation -= data.UpwardAcceleration * Time.fixedDeltaTime;
            if (rotationSpeed > 0) rotationSpeed = 0;
            rotationSpeed -= data.UpwardRotationAcceleration * Time.deltaTime;
        }
        else
        {
            if (rotationSpeed < 0) rotationSpeed = 0;
            rotationSpeed -= data.DownwardRotationAcceleration * Time.deltaTime;

        }
        //else xRotation -= data.DownwardAcceleration * Time.fixedDeltaTime;

        rotationSpeed = Mathf.Clamp(rotationSpeed, -100, 100);
        xRotation += rotationSpeed * Time.deltaTime;
        // Clamp X rotation to min and max values
        xRotation = Mathf.Clamp(xRotation, data.MinXRotation, data.MaxXRotation);

        // Apply new rotation
        birdTransform.rotation = Quaternion.Euler(new Vector3(xRotation, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z));
    }

    // Set drag values based on rotation
    private void UpdateDrag()
    {
        // Rotation == 0 -> Max Drag
        // Rotation == MinRotation || MaxRotation -> Min Drag
        if (xRotation < 0)
        {
            drag = totalSpeed / 2 * Utility.Lerp(drag,
                                               data.MinXRotation,
                                               0,
                                               data.MinDrag,
                                               data.MaxDrag); // Values between [MinRotation, 0] will be converted to [MinDrag, MaxDrag]
        }
        else
        {
            drag = totalSpeed / 2 * -Utility.Lerp(drag,
                                                0,
                                                data.MaxXRotation,
                                                -data.MaxDrag,
                                                -data.MinDrag); // Values between [0, MaxRotation] will be converted to [-MaxDrag, -MinDrag]
        }

        birdRigidbody.drag = drag;
    }

    // Update forward acceleration values based on X rotation
    private void UpdateAcceleration()
    {
        if (xRotation < 0) totalAcceleration = Utility.Lerp(xRotation, data.MinXRotation, 0, data.MinAcceleration, 0);
        //totalAcceleration = data.MinAcceleration;
        else totalAcceleration = Utility.Lerp(xRotation, 0, data.MaxXRotation, 0, data.MaxAcceleration);
        //totalAcceleration = data.MaxAcceleration;
    }

    private void UpdateSpeed()
    {
        // Update forward speed based on acceleration
        totalSpeed += totalAcceleration * Time.deltaTime;
        totalSpeed = Mathf.Clamp(totalSpeed, data.MinSpeed, data.MaxSpeed);

        // Get bird local forward vector
        Vector3 birdForwardVector = birdTransform.forward * totalSpeed;

        //Get birdForwardVector forward and upward values
        forwardSpeed = birdForwardVector.z + 2;
        upwardSpeed = birdForwardVector.y;

        // Update travelling distance
        travelledDistance += forwardSpeed * Time.deltaTime;

        // Move Player's cart transform forward
        transform.Translate(Vector3.forward * forwardSpeed * Time.deltaTime);
        // Move bird's transform in Y axis
        birdTransform.localPosition += new Vector3(0, upwardSpeed, 0) * Time.deltaTime;
    }
}
