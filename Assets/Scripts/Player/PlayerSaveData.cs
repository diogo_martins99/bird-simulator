﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Player Status", menuName = "Scriptable Objects/Player Status")]
public class PlayerSaveData : ScriptableObject
{
    private int Coins { get; set; }
    private float RecordDistance { get; set; }
    private float RecordWallsBroken { get; set; }

    public void UpdateCoinsAmount(int addedAmount) => Coins += addedAmount;
    public void UpdateRecordDistance(float newDistance) => RecordDistance += newDistance;
}
